import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import {BrowserRouter as Rotuer, Switch, Route} from 'react-router-dom'
import Menu from './component/Menu/Menu'
import Main from './component/Main'
import Post from './component/Home/Post'
import Home from './component/Home/Home'
import Video from './component/Video/Video'
import Account from './component/Account/Account'
import Welcome from './component/Auth/Welcome'
import Auth from './component/Auth/Auth'
import { ProtectedRoute } from './component/Auth/ProtectedRoute'



function App() {
    return (
        <div className="App">
            <Rotuer>
                <Menu />
                <Switch>
                    <Route path='/' exact component={Main} />
                    <Route path='/Home' component={Home} />
                    <Route path='/Post/:id' component={Post} />
                    <Route path='/Video' component={Video} />
                    <Route path='/Account' component={Account} />
                    <ProtectedRoute path='/Welcome' component={Welcome} />
                    <Route path='/Auth' component={Auth} />
                </Switch>
            </Rotuer>
        </div>
    )
}

export default App;

