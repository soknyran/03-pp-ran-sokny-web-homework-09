import React from 'react'
import { BrowserRouter as Router, Link, useLocation } from "react-router-dom"

function Account() {
    return (
        <Router>
            <QueryParams />
        </Router>
    )
}

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

function QueryParams() {
    let query = useQuery();
    return (
        <div className="container">
            <p>Account</p>
            <div>
                <ul>
                    <li>
                        <Link to="/account?name=netflix">Netflix</Link>
                    </li>
                    <li>
                        <Link to="/account?name=zillow-group">Zillow Group</Link>
                    </li>
                    <li>
                        <Link to="/account?name=yahoo">Yahoo</Link>
                    </li>
                    <li>
                        <Link to="/account?name=modus-create">Modus Createx</Link>
                    </li>
                </ul>
            </div>
            <Child name={query.get('name')}></Child>
        </div>
    );
}

function Child(props) {
    return (
        <div>
            {props.name ? (
                <p>
                    ID : &quot;{props.name}
                    &quot;
                </p>
            ) : (
                <p>ID : </p>
            )}
        </div>
    );

}

export default Account
