import React from 'react'
import { Form, Button } from "react-bootstrap";
import { useHistory } from 'react-router';
import Check from './Check'

function Auth() {
    let history = useHistory();
    return (
        <div className="container">
            <p>Log In</p>
            <div className="col-sm-5">
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicUsername">
                        <Form.Label>User Name</Form.Label>
                        <Form.Control type="text" placeholder="Username" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicCheckbox">
                        <Form.Check type="checkbox" label="Check me out" />
                    </Form.Group>
                    <Button
                        variant="primary"
                        onClick={() => {
                            Check.login(() => {
                                history.push("/Welcome");
                            });
                        }}
                    >
                        Login
        </Button>
                </Form>
            </div>
        </div>
    )
}

export default Auth
