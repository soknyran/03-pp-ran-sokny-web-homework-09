import React from 'react'
import { Container, Button } from 'react-bootstrap'
import { useHistory } from 'react-router'
import Check from './Check';

function Welcome() {
    let history = useHistory();
    return (
        <div>
            <Container>
                <p>Wellcome</p>
                <Button
                    variant="primary"
                    onClick={() => {
                        Check.logout(() => {
                            history.push("/Auth");
                        });
                    }}
                >
                    Logout
        </Button>
            </Container>
        </div>
    )
}

export default Welcome
