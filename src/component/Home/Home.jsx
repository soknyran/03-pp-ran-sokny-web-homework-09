import React from 'react'
import { Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

function Home() {
    return (
        <div className="container">
            <div className="row mb-2" id="show-card">
                <div className="col-md-3  mb-2">
                    <Card style={{ width: '16rem' }}>
                        <Card.Img variant="top" src="https://futurefilmmaker39480moviereviews.files.wordpress.com/2019/08/avengers_endgame_ver2_xlg.jpg" />
                        <Card.Body>
                            <Card.Title>Avengers</Card.Title>
                            <Card.Text>
                                Avengers: is just ...
                </Card.Text>
                            <Button variant="primary">
                                <Link
                                    style={{ textDecoration: 'none', color: 'white' }}
                                    to={`/Post/1`}>
                                    See more
                                </Link>
                            </Button>
                        </Card.Body>
                    </Card>
                </div>

                <div className="col-md-3">
                    <Card style={{ width: '16rem' }}>
                        <Card.Img variant="top" src="https://futurefilmmaker39480moviereviews.files.wordpress.com/2019/08/spiderman_far_from_home_ver6_xlg.jpg?w=768" />
                        <Card.Body>
                            <Card.Title>Spider-Man</Card.Title>
                            <Card.Text>
                                Spider-Man: is just ...
                </Card.Text>
                            <Button variant="primary">
                                <Link
                                    style={{ textDecoration: 'none', color: 'white' }}
                                    to={`/Post/2`}>
                                    See more
                                </Link>
                            </Button>
                        </Card.Body>
                    </Card>
                </div>

                <div className="col-md-3">
                    <Card style={{ width: '16rem' }}>
                        <Card.Img variant="top" src="https://futurefilmmaker39480moviereviews.files.wordpress.com/2019/08/iron_man_ver4_xlg.jpg" />
                        <Card.Body>
                            <Card.Title>Iron Man</Card.Title>
                            <Card.Text>
                                Iron Man: is just ...
                </Card.Text>
                            <Button variant="primary">
                                <Link
                                    style={{ textDecoration: 'none', color: 'white' }}
                                    to={`/Post/3`}>
                                    See more
                                </Link>
                            </Button>
                        </Card.Body>
                    </Card>
                </div>

                <div className="col-md-3">
                    <Card style={{ width: '16rem' }}>
                        <Card.Img variant="top" src="https://futurefilmmaker39480moviereviews.files.wordpress.com/2019/08/black_panther_ver3_xlg.jpg" />
                        <Card.Body>
                            <Card.Title>Black Panther</Card.Title>
                            <Card.Text>
                                Black Panther: is just ...
                </Card.Text>
                            <Button variant="primary">
                                <Link
                                    style={{ textDecoration: 'none', color: 'white' }}
                                    to={`/Post/4`}>
                                    See more
                                </Link>
                            </Button>
                        </Card.Body>
                    </Card>
                </div>

                <div className="col-md-3">
                    <Card style={{ width: '16rem' }}>
                        <Card.Img variant="top" src="https://futurefilmmaker39480moviereviews.files.wordpress.com/2019/08/doctor_strange_ver3_xlg.jpg" />
                        <Card.Body>
                            <Card.Title>Doctor Strange</Card.Title>
                            <Card.Text>
                            Doctor Strange: is just ...
                </Card.Text>
                            <Button variant="primary">
                                <Link
                                    style={{ textDecoration: 'none', color: 'white' }}
                                    to={`/Post/5`}>
                                    See more
                                </Link>
                            </Button>
                        </Card.Body>
                    </Card>
                </div>

                <div className="col-md-3">
                    <Card style={{ width: '16rem' }}>
                        <Card.Img variant="top" src="https://futurefilmmaker39480moviereviews.files.wordpress.com/2019/08/incredible_hulk_xlg.jpg" />
                        <Card.Body>
                            <Card.Title>The Incredible Hulk</Card.Title>
                            <Card.Text>
                            The Incredible Hulk: is just ...
                </Card.Text>
                            <Button variant="primary">
                                <Link
                                    style={{ textDecoration: 'none', color: 'white' }}
                                    to={`/Post/6`}>
                                    See more
                                </Link>
                            </Button>
                        </Card.Body>
                    </Card>
                </div>

                <div className="col-md-3">
                    <Card style={{ width: '16rem' }}>
                        <Card.Img variant="top" src="https://futurefilmmaker39480moviereviews.files.wordpress.com/2019/08/captain_america_the_first_avenger_ver6_xlg.jpg" />
                        <Card.Body>
                            <Card.Title>Captain America</Card.Title>
                            <Card.Text>
                            Captain America: is just ...
                </Card.Text>
                            <Button variant="primary">
                                <Link
                                    style={{ textDecoration: 'none', color: 'white' }}
                                    to={`/Post/7`}>
                                    See more
                                </Link>
                            </Button>
                        </Card.Body>
                    </Card>
                </div>

                <div className="col-md-3">
                    <Card style={{ width: '16rem' }}>
                        <Card.Img variant="top" src="https://futurefilmmaker39480moviereviews.files.wordpress.com/2019/08/thor_the_dark_world_xlg.jpg" />
                        <Card.Body>
                            <Card.Title>Thor</Card.Title>
                            <Card.Text>
                            Thor: is just ...
                </Card.Text>
                            <Button variant="primary">
                                <Link
                                    style={{ textDecoration: 'none', color: 'white' }}
                                    to={`/Post/8`}>
                                    See more
                                </Link>
                            </Button>
                        </Card.Body>
                    </Card>
                </div>
            </div>
            {/* <div className="Card">
                <Card style={{ width: '16rem' }}>
                    <Card.Img variant="top" src="https://smalltownwashington.com/wp-content/uploads/2020/02/heart-3147976_1920.jpg" />
                    <Card.Body>
                        <Card.Title>Card Title</Card.Title>
                        <Card.Text>
                            Some quick example text to build on the card title and make up the bulk of
                            the card's content.
                </Card.Text>
                        <Button
                            variant="primary"
                            onClick={`/Post/1`}>
                            See more
                            </Button>
                    </Card.Body>
                </Card>
            </div> */}
        </div>
    )
}

export default Home
