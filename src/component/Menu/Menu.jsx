import React from 'react'
import { Navbar, Form, FormControl, Button, Nav } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';

function Menu() {
    return (
        <Navbar bg="light" expand="lg">
            <div className="container">
                <Navbar.Brand as = {Link} to="/">Router-Router</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav
                        className="mr-auto"
                    >
                        <Nav.Link as={Link} to="/home">Home</Nav.Link>
                        <Nav.Link as={Link} to="/video">Video</Nav.Link>
                        <Nav.Link as={Link} to="/account">Account</Nav.Link>
                        <Nav.Link as={Link} to="/welcome">Welcome</Nav.Link>
                        <Nav.Link as={Link} to="/auth">Auth</Nav.Link>
                    </Nav>
                    <Form className="d-flex">
                        <FormControl
                            type="search"
                            placeholder="Search"
                            className="mr-2"
                            aria-label="Search"
                        />
                        <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
            </div>
        </Navbar>
    )
}

export default Menu
