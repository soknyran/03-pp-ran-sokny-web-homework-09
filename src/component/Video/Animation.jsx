import React from 'react'
import { Button, ButtonGroup } from 'react-bootstrap'
import {useParams, Link, useRouteMatch, Switch, Route} from 'react-router-dom'

function Animation() {
    let {path , url} = useRouteMatch();
    return (
        <div className="container">
            <p>Animation Category</p>
            <ButtonGroup aria-label="Basic example">
                    <Button
                        variant="secondary" >
                        <Link
                            style={{ textDecoration: 'none', color: 'white' }}
                            to={`${url}/action`}>
                            Action
                        </Link>
                    </Button>
                    <Button
                        variant="secondary" >
                        <Link
                            style={{ textDecoration: 'none', color: 'white' }}
                            to={`${url}/romance`}>
                            Romance
                        </Link>
                    </Button>
                    <Button
                        variant="secondary" >
                        <Link
                            style={{ textDecoration: 'none', color: 'white' }}
                            to={`${url}/comedy`}>
                            Comedy
                        </Link>
                    </Button>
                </ButtonGroup>
            <Switch>
                <Route exact path={path}>
                    <p>Please Choose Category : </p>
                </Route> 
                <Route path={`${path}/:animationId`}>
                    <AnimaionCat/>
                </Route>
            </Switch>
        </div>
    );
}

function AnimaionCat(){
    let {animationId} = useParams();
    return(
        <div>
            <p>Please Choose Category : {animationId}</p>
        </div>
    )
}

export default Animation
