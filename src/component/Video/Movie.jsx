import React from 'react'
import { Button, ButtonGroup } from 'react-bootstrap'
import {useParams, Link, useRouteMatch, Switch, Route} from 'react-router-dom'

function Movie() {
    let {path , url} = useRouteMatch();
    return (
        <div className="container">
            <p>Movie Category</p>
            <ButtonGroup aria-label="Basic example">
                    <Button
                        variant="secondary" >
                        <Link
                            style={{ textDecoration: 'none', color: 'white' }}
                            to={`${url}/advanture`}>
                            Advanture
                        </Link>
                    </Button>
                    <Button
                        variant="secondary" >
                        <Link
                            style={{ textDecoration: 'none', color: 'white' }}
                            to={`${url}/crime`}>
                            Crime
                        </Link>
                    </Button>
                    <Button
                        variant="secondary" >
                        <Link
                            style={{ textDecoration: 'none', color: 'white' }}
                            to={`${url}/action`}>
                            Action
                        </Link>
                    </Button>
                    <Button
                        variant="secondary" >
                        <Link
                            style={{ textDecoration: 'none', color: 'white' }}
                            to={`${url}/romance`}>
                            Romance
                        </Link>
                    </Button>
                    <Button
                        variant="secondary" >
                        <Link
                            style={{ textDecoration: 'none', color: 'white' }}
                            to={`${url}/comedy`}>
                            Comedy
                        </Link>
                    </Button>
                </ButtonGroup>
            <Switch>
                <Route exact path={path}>
                    <p>Please Choose Category : </p>
                </Route> 
                <Route path={`${path}/:movieId`}>
                    <MovieCat/>
                </Route>
            </Switch>
        </div>
    );
}

function MovieCat(){
    let {movieId} = useParams();
    return(
        <div>
            <p>Please Choose Category : {movieId}</p>
        </div>
    )
}

export default Movie
