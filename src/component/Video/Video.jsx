import React from 'react'
import { Button, ButtonGroup } from 'react-bootstrap'
import { Link, Switch, Route, BrowserRouter as Router } from 'react-router-dom'
import Animation from './Animation'
import Movie from './Movie';

function Video() {
    
    return (
        <Router>
            <div className="container">
                <p>Video</p>
                <ButtonGroup aria-label="Basic example">
                <Button
                        variant="secondary" >
                        <Link
                            style={{ textDecoration: 'none', color: 'white' }}
                            to="/.video/movie">
                            Movie
                        </Link>
                    </Button>
                    <Button
                        variant="secondary" >
                        <Link
                            style={{ textDecoration: 'none', color: 'white' }}
                            to="/.video/animation">
                            Animation
                        </Link>
                    </Button>
                </ButtonGroup>
            </div>
            <Switch>
                <Route path="/.video/animation">
                    <Animation />
                </Route>
                <Route path="/.video/movie">
                    <Movie />
                </Route>
            </Switch>
        </Router>
    );
}

export default Video
